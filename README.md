# Optimal_Switch_Time_Boost

Rees Hatch ECE7360 Final Project: Optimal Switch Time Boost Converter


Instructions for running Optimal_Switch_Time_Boost.m

Step 1.		Download the file "Optimal_Switch_Time_Boost.m"

Step 2.		Open the file in Matlab and make sure it is included in the path

Step 3.		Run the file



Notes:

To get the results from the half gird period simulations ensure that the following parameters defined correctly in Problem Setup,

a) Fs = 5e3
b) N_grid = 0.5
c) L = 200e-6

To get the results from the 3 gird period simulations ensure that the following parameters defined correctly in Problem Setup,

a) Fs = 2e3
b) N_grid = 3.0
c) L = 1e-3

To select the different solving approahes change the variable, Solving_Approach,

1) Approach 1: 'trust-region-reflective' with gradient
2) Approach 2: 'trust-region-reflective' with gradient + 'sqp' 
3) Approach 3: 'sqp' 
4) Approach 4: 'sqp' with gradient 