function Optimal_Switch_Time_Boost
close all;
    %% Problem setup
    % Initialize problem parameters
    % Time Paramters
    P.Solving_Approach = 2; % Selects which alorithim(s) to solve with
    P.Fs = 5e3;        % Switching frequency
    P.Ts = 1/P.Fs;      % Switching period
    
    P.Fg = 50;          % Grid frequency
    P.Tg = 1/P.Fg;      % Grid period
    P.N_grid = 0.5;     % Number of grid cycles to simulate
    P.N = P.N_grid*P.Fs/P.Fg; % Number of switching periods
    P.tau_max = 0.99*P.Ts;      % Upper bound on tau
    P.tau_min = 0.01*P.Ts;      % Lower bound on tau

    % Circuit Paramters
    P.R_L = 30e-3;  % Parastic resistance of inductor
    P.L = 200e-6;   % Input iductor 
    P.C = 100e-3;   % Output filter capacitor
    P.R_Load = 4;   % Output resistance 
    
    % Operation Point 
    P.Vm = 120*sqrt(2);                 % Input voltage magnitude
    P.v_d = 200;                        % Output voltage reference
    P.R_e = (P.Vm/P.v_d)^2*P.R_Load/2;  % Emulated AC resistance
    P.x0 = [P.Vm/P.R_e; P.v_d];         % Initial state
    
    % Indexing Variables
    P.n_x = 2;              % Number of states
    P.n_u = 1;              % Number of inputs
    P.n_state = P.n_x*P.N*2;% Number of states combined 
    P.n_ctrl = P.n_u*P.N;   % Number of inputs combined
    
    % Cost Variables
    P.q1 = 1e3;             % Instantaneous wieght on current error
    P.q2 = 0;               % Instantaneous wieght on voltage error
    P.Q = diag([P.q1 P.q2]);% Instantaneous Wieght Matrix 
    P.s1 = 1;               % Terminal wieght on current error
    P.s2 = 1;             % Terminal wieght on voltage error
    P.S = diag([P.s1 P.s2]);% Penalty on terminal state
   
    % Define Continuous Dynamics
    [P.A1, P.A2, P.B] = calculateContinuousTimeMatrices(P);
    P.inv_A1 = eye(2)/P.A1; % Define inverse A1 to save compution later
    P.inv_A2 = eye(2)/P.A2; % Define inverse A2 to save compution later
    % Calculate discrete dynamics
    [P.Abar1, P.Abar2] = calculateDiscreteTimeMatrices(P);
    
    % Initialize Input Voltage Waveform and Desired Current Trajectory
    [P.u, P.x_d] = initializeInputAndReference(P);
    
    % Initialize a guess for tau  
    tau0 = initializeTau(P);
    
    % Simulate the initial state
%     tic
    x0 = discreteSim(tau0, P); % Simulate forward in time using the control laws and switch times
%     time_sim = toc

    %% Optimize
    tic
    x = x0; tau = tau0; c_init = costSequential(tau0, P)
    
    if P.Solving_Approach == 1
        [x, tau] = sequentialOptimization(tau, P, true, 1);
    elseif P.Solving_Approach == 2
        [~, tau] = sequentialOptimization(tau, P, true, 1);
        [x, tau] = sequentialOptimization(tau, P, false, 2);
    elseif P.Solving_Approach == 3
        [x, tau] = sequentialOptimization(tau, P, false, 2);
    elseif P.Solving_Approach == 4
        [x, tau] = sequentialOptimization(tau, P, true, 2);
    end
    time_opt = toc
      
    %% Plot the result
    % Reformat the state and control to have one column per time index
    x_mat = [P.x0 reshape(x, P.n_x, P.N*2)]; % Add in the initial state as well
    x0_mat = [P.x0 reshape(x0, P.n_x, P.N*2)]; % Add in the initial state as well
    x_d_mat = reshape(P.x_d, P.n_x, P.N+1); % Includes the final desired state
    u_mat = reshape(P.u, P.n_u, P.N+1); % Includes the final input voltage 
  
    % Create Time axis for plotting states and inputs
    t_d = (0:P.N)*P.Ts;         % Time vector for plotting desired state and input
    t_tau = zeros(1, P.N*2+1);  % Time vector for plottting final state x_mat
    t_tau0 = zeros(1, P.N*2+1); % Time vector for plotting initial state x0_mat
    % Loop over tau to construct time vector 
    for k = 1:P.N*2+1
        if(mod(k,2))                            % if k odd 
            t_tau(k) = (k-1)/2*P.Ts;            % time is an integer multiple
            t_tau0(k) = (k-1)/2*P.Ts;           % of Ts
        else                                    % else is k even
            t_tau(k) = t_tau(k-1)+tau(k/2);     % time is an integer multiple 
            t_tau0(k) = t_tau0(k-1)+tau0(k/2);  % of Ts + tau_k
        end
    end
   
    % Plot the states
    figure;
    names = {'I_L', 'V_C', 'd'};
    for k = 1:2
        subplot(3, 1, k);
        plot(1000.*t_d, x_d_mat(k,:), 'r:', 'linewidth', 3); hold on;
        plot(1000.*t_tau, x_mat(k,:), 'b', 'linewidth', 2); 
        xlim([0 P.N*P.Ts*1000]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k});
    end
    subplot(3, 1, 3);
    plot(t_d*1000, 1/P.Ts.*[tau' tau(end)], 'b', 'linewidth', 2);
    xlim([0 P.N*P.Ts*1000]); % Ensures the plot has the same x-axis as the others
    ylabel(names{3});
    
    % Plot the original states
    figure;
    names = {'I_L', 'V_C', 'd'};
    for k = 1:2
        subplot(3, 1, k);
        plot(1000.*t_d, x_d_mat(k,:), 'r:', 'linewidth', 3); hold on;
        plot(1000.*t_tau0, x0_mat(k,:), 'b', 'linewidth', 2); 
        xlim([0 P.N*P.Ts*1000]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k});
    end
    subplot(3, 1, 3);
    plot(t_d*1000, 1/P.Ts.*[tau0' tau0(end)], 'b', 'linewidth', 2);
    xlim([0 P.N*P.Ts*1000]); % Ensures the plot has the same x-axis as the others
    ylabel(names{3});
    ylim([0 1]);
    % Plot the inputs and duty cycle 
    figure;
    names = {'V_g', 'd'};
    for k = 1:P.n_u
%         subplot(P.n_u+1, 1, k);
        plot(1000.*t_d, u_mat(k,:), 'b', 'linewidth', 2);
        xlim([0 P.N*P.Ts*1000]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k});
    end

    %% Output data
    tau = [t_d; tau' tau(end)];
    save('tau.mat','tau');
    
end


%% Optimization functions %%
function [x, tau] = sequentialOptimization(tau0, P, Gradient, Algorithm)
% sequentialOptimization optimizes over the switch times, tau. 
%
% Inputs:
%   tau0: Initial guess of the optimal switch times
%   P: Struct of parameters
%   Gradient: [boolean] specifies whether the anaylitcal gradient is used by
%   optimizer
%   Algorithm: selects the Algorithim to be used by the optimizer
%
% Outputs:
%   tau: Optimized switch time (tau_1 to tau_N in a single column vector)
    
    % Initialize the solve variables
%     options = optimoptions(@fmincon, 'Algorithm', 'sqp'); % choose sequential-quadratic-programming
%     options = optimoptions(@fmincon, 'Algorithm', 'interior-point'); % choose an the interior-point algorithm
%     options = optimoptions(@fmincon, 'Algorithm', 'trust-region-reflective');

    if Algorithm==1
        options = optimoptions(@fmincon, 'Algorithm', 'trust-region-reflective');
    elseif Algorithm==2
        options = optimoptions(@fmincon, 'Algorithm', 'sqp'); 
%         options = optimoptions(@fmincon, 'Algorithm', 'interior-point');
    end
    
    % Max Function Evalutions before optimizer gives up
    options = optimoptions(options, 'MaxFunctionEvaluations', 100e3);
    % Indicate whether to use gradients (Note that there are no constraint gradients)
    options = optimoptions(options, 'SpecifyObjectiveGradient', Gradient);
    % Tolerance for optimization
    options = optimoptions(options, 'OptimalityTolerance', 0.01); 
    % Have Matlab display the optimization information with each iteration
    options.Display = 'iter'; 
    
    % These options can be used to evauate whether or not the gradient
    % matches the optimizers estimated gradient
    options = optimoptions(options, 'FunctionTolerance', 1e-9);
%     options = optimoptions(options, 'CheckGradients', true);
%     options = optimoptions(options, 'FiniteDifferenceStepSize', 1e-14);
%     options = optimoptions(options, 'FiniteDifferenceType', 'central');
    
    A = []; % No linear inequality constraints 
    B = []; % No linear inequality constraints 
    Aeq = []; % No equality constraints
    Beq = [];  % No linear inequality constraints 
    
    % Define the upper and lower bounds
    lb = P.tau_min*ones(1,P.N);
    ub = P.tau_max*ones(1,P.N); 
    
    % Matlab call:
    optimize = true; % If optimize is true, then it will optimize. Otherwise it will load in the data
    if optimize
        [tau, final_cost] = fmincon(@(tau_in) costSequential(tau_in, P), tau0, A, B, Aeq, Beq, lb, ub, [], options);
        save('Data\sequential_data.mat');
    else
        data = load('Data\sequential_data.mat');
        u = data.u;
        final_cost = data.final_cost;
    end
    disp(['Final cost = ' num2str(final_cost)]);
    
    % Simulate the state forward in time to be able to output the result
    x = discreteSim(tau, P);    
end

%% Cost functions %%
function [c, dc_dtau] = costSequential(tau, P)
%costSequential calculates the cost and gradient of the cost with respect to
%               the switch times, tau
%
% Inputs:
%   tau: Switch times (tau_1 to tau_N in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   c: cost
%   dc_dtau: partial derivative of the cost wrt tau

    % Calculate the state based upon the controls
    x = discreteSim(tau, P);
    
    % Calcualte the cost
    c = cost(tau, x, P);
    
    % Calculate the partial
    if nargout > 1
        dc_dtau = sequentialPartial(tau, x, P);        
    else
        dc_dtau = [];
    end
end

function c = cost(tau, x, P)
%cost calculates the cost given the switch times and corresponding state
%     trajectory
%
% Inputs:
%   tau: Switch times (tau_1 to tau_N in a single column vector)
%   x: State (x_1 to x_N in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Loop through the control inputs to calculate the cost
    c = 0; % Initialize the cost as zero
    
    ind_state = 1:P.n_x;
    ind_d = 1:P.n_x;
    x_kp2 = P.x0;
    
    % Calaculate the instantanous cost
    for k = 1:(P.N) % Loop over the switching periods       
        % Extract switch time
        tau_k = tau(k); % Switch time for iteration k
        
        % Extract the states
        x_k = x_kp2; % State for iteration k
        x_kp1 = x(ind_state); % State for iteration k+1
        ind_state = ind_state + P.n_x; % Update the state indicies
        x_kp2 = x(ind_state); % State for iteration k+2
        ind_state = ind_state + P.n_x; % Update the state indicies
        
        % Extract the desired states
        xd_k = P.x_d(ind_d); % Desired state for iteration k 
        ind_d = ind_d + P.n_x; % Update the desired state indicies
        xd_kp2 = P.x_d(ind_d); % Desired state for iteration k+2
        
        % Interpolate the desired state at the swtich time
        dxd_dt = (xd_kp2-xd_k)/P.Ts;    % Change in x_d 
        xd_kp1 = xd_k + tau_k*dxd_dt;   % x_d(kTs+tau_k)
        
        % Calculate state errors
        xe_k = x_k - xd_k;              % state error at start of period
        xe_kp1 = x_kp1 - xd_kp1;        % state error at swtich time
        xe_kp2 = x_kp2 - xd_kp2;        % state error at end of period
        
        % Calculate change in state error
        dxe_dt = (xe_kp1-xe_k)/tau_k; % change in state over t=(0, tau_k)   
        dxe_kp1_dt = (xe_kp2-xe_kp1)/(P.Ts-tau_k); % change in state over t=(tau_k,Ts)

        % Approximate the integral of state error  for each state
        % individully
        % i1 => inductor current error from t=(0, tau_k)
        % i2 => capacitor voltage error from t=(0, tau_k)
        % i3 => inductor current error from t=(tau_k,Ts)
        % i4 => capacitor voltage error from t=(tau_k,Ts)
        
        if dxe_dt(1)~=0 % check for constant error 
            i1 = 1/3/dxe_dt(1)*((xe_kp1(1))^3-(xe_k(1))^3);
        else
            i1 = xe_k(1)^2*tau_k;
        end
        if dxe_dt(2)~=0 % check for constant error
            i2 = 1/3/dxe_dt(2)*((xe_kp1(2))^3-(xe_k(2))^3);
        else
            i2 = xe_k(2)^2*tau_k;
        end
        if dxe_kp1_dt(1)~=0 % check for constant error
            i3 = 1/3/dxe_kp1_dt(1)*((xe_kp2(1))^3-(xe_kp1(1))^3);
        else
            i3 = xe_kp1(1)^2*(P.Ts-tau_k);
        end
        if dxe_kp1_dt(2)~=0 % check for constant error
            i4 = 1/3/dxe_kp1_dt(2)*((xe_kp2(2))^3-(xe_kp1(2))^3);
        else
            i4 = xe_kp1(2)^2*(P.Ts-tau_k);
        end
        
        % Add the integrals together using the cost wieghts from Q and add
        % the result to the running cost
        c = c + [P.q1 P.q2]*([i1; i2]+[i3; i4]);
        
    end
    
    % Calculate terminal cost
    x_k = x_kp2; % State for iteration k
    xd_k = P.x_d(ind_d); % Desired state for iteration k 

    x_e = (x_k - xd_k); % Terminal error from the final desired value
    ct = x_e'*P.S*x_e;  % Terminal cost
    
    % Add Terminal cost to total cost
    c = c + ct; % Quadratic in the error
end
 
function dc_dtau = sequentialPartial(tau, x, P)
%sequentialPartial: Calculates the partial of the cost wrt the switch times
%
% Note that the partial takes the form:
%       dc/dtau_k = lam_{k+1}^T (A1-A2) x_{k+1}
%
% Inputs: 
%   tau: Switch times (tau_1 to tau_N in a single column vector)
%   x: State (x_1 to x_N in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   dc_dtau: Partial of cost wrt the switch times
%
    % Initialize the partial
    dc_dtau = zeros(1, P.N); % We will use one column per gradient and then reshape to make a row vector at the end
    
    % Reshape vectors for easy access
    x_mat = [P.x0 reshape(x, P.n_x, P.N*2)]; % Also add in the initial state
    xd_mat = reshape(P.x_d, P.n_x, P.N+1); % Includes the final desired state 
   
    % Initialize index
    ind_x = size(x_mat, 2); % Final column index corresponds to the final state
    ind_d = size(xd_mat, 2); % Final column index corresponds to the final desired state
    ind_tau = size(tau,1); % Final column index corresponds to final switch time
 
    % Initialize final lagrange multiplier (lam_N = dphi/dx(x_N)
    xk = x_mat(:, ind_x);
    xdk = xd_mat(:,ind_d);
    xek = xk-xdk;
    lam_kp2 = (2.*(xek)'*P.S)'; % dphi/dx(x_N) <-- This variable is used as lam_{k+2}
    
    % Update indicies
    ind_x = ind_x - 1; % Index of x_k at iteration 2N-1 
    ind_d = ind_d - 1; % Index of xd_k at iteration N-1 
    
    % Simulate backwards in time (achived by moving indicies backwards)
    for k = 1:P.N 
        % Extract the switch time
        tau_k = tau(ind_tau); % Switch time at iteration k
        
        % Extract the new states and update the indicies
        xkp1 = x_mat(:, ind_x); % State at iteration k+1
        ind_x = ind_x - 1;
        xk = x_mat(:, ind_x); % State at iteration k
        ind_x = ind_x - 1;
        
        % Extract the desired state and update the indicies
        xdkp2 = xdk;
        xdk = xd_mat(:,ind_d);
        ind_d = ind_d - 1;
        
        % Interpolate xd_{k+1} from xd_k and xd_{k+2}
        dxd_dt = (xdkp2-xdk)/P.Ts;
        xdkp1 = xdk + dxd_dt*tau_k;        
        
        % Calculate state errors and discretization intervals
        xekp2 = xek;
        xekp1 = xkp1 - xdkp1;
        xek = xk - xdk;
        dtkp1 = P.Ts - tau_k;
        dtk = tau_k;
        dxekp1_dt = (xekp2-xekp1)/dtkp1;
        dxek_dt = (xekp1-xek)/dtk;
        
        % Calculate lam_kp1
        eAt = P.Abar2(dtkp1);
%         lam_kp1 = eAt*lam_kp2+(eAt-eye(2))*P.Q*(xekp1+xekp2);
        lam_kp1 = eAt*lam_kp2+2.*P.inv_A2*(eAt*P.Q*xekp2-P.Q*xekp1-P.inv_A2*(eAt-eye(2))*P.Q*dxekp1_dt);
        
        % Calculate lam_k
        eAt = P.Abar1(dtk);
%         lam_k = eAt*lam_kp1+(eAt-eye(2))*P.Q*(xek+xekp1);
        lam_k = eAt*lam_kp1+2.*P.inv_A1*(eAt*P.Q*xekp1-P.Q*xek-P.inv_A1*(eAt-eye(2))*P.Q*dxek_dt);

        % Reset lam_{k+2} for the next loop to use it as intial condition
        lam_kp2 = lam_k;
               
        % Calculate partial        
        dc_dtau(:, ind_tau) = (lam_kp1.')*(P.A1-P.A2)*xkp1; 
        ind_tau = ind_tau - 1; % Update switch time index
        
    end
    
    % Reshape partial to be proper output
    dc_dtau = reshape(dc_dtau, 1, P.N); % changes it from partial in columns for each iteration to one single row
end


%% Simulation functions %%
function x = discreteSim(tau, P)
% discreteSim calculates the state given the switch times
%
% Inputs:
%   tau: Switch times (tau_1 to tau_N in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Calculated state (x_1 to x_N in a single column vector)
%
    x = zeros(P.n_state, 1); % Initialize the output states
    
    % Initialize the indicies
    ind_tau = 1;        % Indices for the switching cycle
    ind_state = 1:P.n_x;% Indices for the state
    ind_ctrl = 1:P.n_u; % Indices for the input
    
    % Initialize variables for the forward simulation
    xkp2 = P.x0; % Set x_{k+2} to the initial state for initialization purposes
    ukp2 = P.u(ind_ctrl); % Set u_{k+2} to the initial control for initialization purposes
    ind_ctrl = ind_ctrl+P.n_u; % Update the control indicies
    
    % Calculate the simulation forward in time
    for k = 1:P.N          
        % Update Switch time and control
        xk = xkp2;                  % State at iteration k
        tau_k = tau(ind_tau);       % Switch time for iteration k
        ind_tau = ind_tau + 1;      % Update switch time index
        uk = ukp2;                  % Input at iteration k
        ukp2 = P.u(ind_ctrl);       % Input at iteration k+2
        ind_ctrl = ind_ctrl+P.n_u;  % Update control indicies
        
        % Calculate FOH of control input
        du_dt = (ukp2-uk)/P.Ts;     % Change in control over switching period
        ukp1 = uk + du_dt*tau_k;    % Input at iteration k+1
        
        % Calculate State x_kp1
        eAt = P.Abar1(tau_k);       % Calculate e^(A_1*tau) 
        xkp1 = eAt*xk - P.inv_A1*(P.B*ukp1-eAt*P.B*uk-P.inv_A1*(eAt-eye(2))*P.B*du_dt);
        x(ind_state) = xkp1;        % Record state calcultion
        ind_state = ind_state + P.n_x; % Update state indicies
        
        % Calculate State x_kp2
        eAt = P.Abar2(P.Ts-tau_k);  % Calculate e^(A_1*(Ts-tau))
        xkp2 = eAt*xkp1 - P.inv_A2*(P.B*ukp2-eAt*P.B*ukp1-P.inv_A2*(eAt-eye(2))*P.B*du_dt);
        x(ind_state) = xkp2;        % Record state calculation
        ind_state = ind_state + P.n_x; % Update state indicies   
    end
end

function [A1, A2, B] = calculateContinuousTimeMatrices(P)
% calculateContinuousTimeMatrices defines system matricies for the
%                                 system in states 1 and 2
%
% Inputs:
%   P: Struct of parameters
%
% Outputs:
%   A1: System matrix for state 1 
%   A2: System matrix for state 2
%   B:  Input matrix for input V_grid
%   
    a = -P.R_L/P.L;         % entry 1,1 of A1&A2
    b = -1/P.L;             % entry 1,2 of A2                 
    c =  1/P.C;             % entry 2,1 of A2
    d = -1/(P.R_Load*P.C);  % entry 2,2 of A1&A2
    
    A1 = [a 0; 0 d];
    A2 = [a b; c d];
    B = [1/P.L; 0];
end

function [Abar1, Abar2] = calculateDiscreteTimeMatrices(P)   
% calculateDiscreteTimeMatrices defines matrix exponetials for the
%                               system in states 1 and 2
%
% Inputs:
%   P: Struct of parameters
%
% Outputs:
%   Abar1: Function handle of e^{A_1dT}(dT) 
%   Abar2: Function handle of e^{A_2dT}(dT)
%
    a = P.A2(1,1);  % entry 1,1 of A1&A2
    b = P.A2(1,2);  % entry 1,2 of A2
    c = P.A2(2,1);  % entry 2,1 of A2
    d = P.A2(2,2);  % entry 2,2 of A1&A2
    
    p1 = (a+d)/2;
    p2 = (a-d)/2;
    eP1t = @(dt) exp(p1*dt);
    
    Abar1 = @(dt) eP1t(dt)*[(1+p2*dt) 0; 0 (1-p2*dt)];   
    Abar2 = @(dt) eP1t(dt)*[(1+p2*dt) b*dt; c.*dt (1-p2.*dt)];
    
end


%% Initialization functions %%
function [u, x_d] = initializeInputAndReference(P)   
% initializeInputAndReference create input and desried trajectory vectors
%
% Inputs:
%   P: Struct of parameters
%
% Outputs:
%   u: discretized input voltage vector (u_1 to u_N/2 in a single column vector)
%   x_d: discretized desired state trajectory (xd_1 to xd_N in a single column vector)
%
    u = zeros(P.n_ctrl, 1);
    x_d = zeros(P.n_state/2,1);
    ind_ctrl = 1:P.n_u;     % Indices for the control 
    ind_state = 1:P.n_x;    % Indices for the state
    Vg = @(t) P.Vm*abs(cos(2*pi*P.Fg*t)); % Function handle continous input voltage
    
    % Loop over the length of the simualtion
    for k = 1:(P.N+1)
        u(ind_ctrl) = Vg(P.Ts*(k-1));
        x_d(ind_state) = [u(ind_ctrl(1))/P.R_e; P.v_d];
        ind_ctrl = ind_ctrl + P.n_u;
        ind_state = ind_state + P.n_x;
    end    
end

function [tau0] = initializeTau(P)   
% initializeTau defines an intial guess of optimal
%               switch times 
%      
%
% Inputs:
%   P: Struct of parameters
%
% Outputs:
%   tau0: Initial guess of switch times (tau0_1 to tau0_N in a single column vector)
%
% Initialize variables for optimization

  % Initialize variables for optimization
    tau0 = (P.Ts*0.2).*ones(P.N, 1);    % Note that a zero-input control corresponds 
                                        % to a local minimum that the optimization has a 
                                        % hard time moving out of                                
  % Load tau0 from switching simulation  
%     load('tau0.mat', 'tau0');
%     i = 1:(P.N);
% %     i = (1:(P.N))+200*19.5;
%     tau0 = tau0(2,i)';
                                  
  % Create Tau from FF Control   
%     for k = 1:(P.N)
%         tau0(k) = (1-P.u(k)/P.v_d)*P.Ts;
%         if tau0(k) > P.tau_max
%             tau0(k) = P.tau_max;
%         elseif tau0(k) < P.tau_min
%             tau0(k) = P.tau_min;
%         end
%     end
%     
end